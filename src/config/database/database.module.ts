import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@/config/config.module';
import { ConfigService } from '@/config/config.service';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (config: ConfigService) => {
                return {
                    type: 'postgres',
                    host: config.get('DB_HOST'),
                    port: Number(config.get('DB_PORT')),
                    username: config.get('DB_USERNAME'),
                    password: config.get('DB_PASSWORD'),
                    database: config.get('DB_DATABASE'),
                    // keepConnectionAlive: true,
                    entities: [__dirname + '/../../app/entity/*{.ts,.js}'],
                    logging: config.getBoolean('DB_LOGGING'),
                    options: { useUTC: true, instanceName: config.get('DB_INSTANCE'), encrypt: config.getBoolean('DB_ENCRYPT') },
                    synchronize: true,
                };
            },
        }),
    ],
})
export class DatabaseModule {}
