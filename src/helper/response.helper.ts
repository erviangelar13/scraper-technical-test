import { HttpException, HttpStatus } from '@nestjs/common';

export class Response {
    code: HttpStatus;
    message: string;
    data: any;
    timestamp: string;

    constructor(message: string, data: any, code: HttpStatus = HttpStatus.OK, timestamp: string = Date().toLocaleString()) {
        this.message = message;
        this.data = data;
        this.code = code;
        this.timestamp = timestamp;
    }
}

export const response = (message: string, data: any = null) => new Response(message, data);

export const responseError = (message: string, code: HttpStatus = HttpStatus.BAD_REQUEST) => {
    return new HttpException({ message: message, code: code, timestamp: Date().toLocaleString() }, code);
};
