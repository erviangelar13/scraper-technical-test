import { Injectable } from '@nestjs/common';
import { UserEntity } from '../../app/entity/user.entity';
import { UserDto, UserResponse } from '../../app/users/users.dto'
import { AuthDto } from '../../app/auth/auth.dto'

@Injectable()
export class UserService {
    
    async getAll() : Promise<UserResponse[]> {
        try {
            return (await UserEntity.createQueryBuilder().getMany()).map(x => {
                return UserResponse.from(x);
            });
        } catch (err) {
            throw new Error(err.message);
        }
    }

    async getById(id: number) {
        try {
            const data = await UserEntity.createQueryBuilder().
                where(`id = :id`,
                    { id: id }
                ).
                getOne();

            if (!data) throw new Error('Data not found');
            return UserResponse.from(data);
        } catch (err) {
            throw new Error(err.message);
        }
    }

    async update(id: number,dto: UserDto) {
        try {
            return await UserEntity.createQueryBuilder().
                update().
                set({
                    email: dto.email,
                    name: dto.name,
                    password: dto.password,
                    phone: dto.phone,
                }).
                where('id = :id', {
                    id: id,
                }).
                execute();
        } catch (err) {
            throw new Error(err.message);
        }
    }

    async delete(id: number) {
        try {
            return await UserEntity.createQueryBuilder().
                delete().
                where('id = :id', {
                    id: id,
                }).
                execute();
        } catch (err) {
            throw new Error(err.message);
        }
    }
}
