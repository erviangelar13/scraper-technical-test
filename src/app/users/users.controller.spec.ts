import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import { UserController } from './users.controller';
import { UserService } from './users.service';
import { UserDto, UserResponse } from './users.dto';
import { Config } from '../../helper/config.helper';

describe('AuthController', () => {
  let controller: UserController;
  let spyService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
        imports:[
            JwtModule.register({
                secret: Config.get('SECRET_KEY'),
                signOptions: { expiresIn: Config.get('EXPIRED') },
            })
            ],
        controllers: [UserController],
        providers: [UserService]
    }).compile();

    controller = module.get<UserController>(UserController);
    spyService = module.get<UserService>(UserService);
  });

  it('update', async () => {
    const param : UserDto = {
        email: "ayesha.d@gmail.com",
        name: "Ayesha Noor D",
        password: "@yesh4",
        phone: "081246889967"
    };
    // await controller.register(param);
    expect(controller.update(1, param)).toBeDefined();
  });

  it('update', async () => {
    const param : UserDto = {
        email: "",
        name: "Ayesha Noor D",
        password: "@yesh4",
        phone: "081246889967"
    };
    await controller.update(-2, param);
    // await controller.register(param);
    expect(spyService.update(-2, param)).toHaveBeenCalled();
  });

  it('delete', async () => {
    await controller.delete(1);
    expect(spyService.delete(1)).resolves.toBeCalled();
  });

});
