import { IsAlphanumeric, IsEmail, IsNotEmpty, IsNumber, IsPhoneNumber } from 'class-validator';
import { UserEntity } from '../../app/entity/user.entity'
import { ApiProperty } from '@nestjs/swagger';
export class UserDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;

    @ApiProperty()
    @IsAlphanumeric()
    name: string;

    @ApiProperty()
    @IsPhoneNumber()
    phone: string;
}

export class UserResponse {
    @IsNumber()
    id: number;

    @IsNotEmpty()
    email: string;

    @IsAlphanumeric()
    name: string;

    @IsPhoneNumber()
    phone: string;

    public static from(item: UserEntity) {
        const it = new UserResponse();
        it.id = item.id;
        it.name = item.name;
        it.email = item.email;
        it.phone = item.phone;
        return it;
    }
}