import { Controller, Get, Post, Put, Delete, Param, Req, Body } from '@nestjs/common';
import { response, responseError } from '../../helper/response.helper';
import { UserService } from './users.service';
import { UserDto } from './users.dto'
import { ApiProperty, ApiBody } from '@nestjs/swagger';

@Controller('users')
export class UserController {
    
    constructor(
        private _userService: UserService,
    ) { }

    @Get()
    async getAll() {
        try {
            const data = await this._userService.getAll();
            return response('success', data);
        } catch (err) {
            return responseError(err.message);
        }
    }

    @Get(':id')
    async getById(@Param('id') id: number) {
        try {
            const data = await this._userService.getById(id);
            return response('success', data);
        } catch (err) {
            return responseError(err.message);
        }
    }

    @Put(':id')
    async update(@Param('id') id: number, @Body() dto: UserDto) {
        try {
            await this._userService.update(id, dto);
            return response('Success update', true);
        } catch (err) {
            return responseError(err.message);
        }
    }

    @Delete(':id')
    async delete(@Param('id') id: number) {
        try {
            await this._userService.delete(id);
            return response('Success delete', true);
        } catch (err) {
            return responseError(err.message);
        }
    }
}
