import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto, RegisterDto, AuthResponse } from './auth.dto';
import { response, responseError } from '../../helper/response.helper';

@Controller('auth')
export class AuthController {

    constructor(
        private _authService: AuthService,
    ) { } 

    @Post('login')
    async login(@Body() dto: AuthDto) {
        try {
            const res = await this._authService.login(dto);
            return response('success', res);
        } catch (err) {
            return err; //responseError(err.message);
        }
    }

    @Post('register')
    async register(@Body() dto: RegisterDto) {
        try {
            const res = await this._authService.register(dto);
            return response('success', res);
        } catch (err) {
            return err; //responseError(err.message);
        }
    }
}
