import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from '../entity/user.entity';
import { RegisterDto, AuthDto, AuthResponse, RegisterReponse } from './auth.dto'

@Injectable()
export class AuthService {
    constructor(private _jwtService: JwtService) { }
    
    async login(dto: AuthDto) {
        try {
            return await UserEntity.findOne({ email : dto.email }).then((value) => {
                if (!value) throw new Error(`User with email = ${dto.email} is not found`);
                
                var exist = UserEntity.findOne({ email: dto.email, password: dto.password });
                if (!exist) throw new Error(`Passsword for email = ${dto.email} is wrong`);
                
                const token = this._jwtService.sign({
                    email: value.email,
                    name: value.name
                }),
                userData = { id: value.id, name: value.name, token: token };
                return userData;
            });
        } catch (err) {
            throw new Error(err.message);
        }
    }
    
    async register(dto: RegisterDto) : Promise<RegisterReponse> {
        try {

            if (dto.email) throw new Error(`email cannot be empty`);
            var user = await UserEntity.findOne({ email : dto.email });
            if (user) throw new Error(`User with email = ${dto.email} is already exist`);
            var register = await UserEntity.createQueryBuilder().
                insert().
                values({
                    email: dto.email,
                    password: dto.password,
                    name: dto.name,
                    phone: dto.phone
                }).
                execute();
            let response = new RegisterReponse();
            response.id = register.identifiers[0].id;
            return response;
        } catch (err) {
            throw new Error(err.message);
        }
    }
}
