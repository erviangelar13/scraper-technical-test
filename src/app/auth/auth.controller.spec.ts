import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthDto, RegisterDto } from './auth.dto';
import { Config } from '../../helper/config.helper';

describe('AuthController', () => {
  let controller: AuthController;
  let spyService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
        imports:[
            JwtModule.register({
                secret: Config.get('SECRET_KEY'),
                signOptions: { expiresIn: Config.get('EXPIRED') },
            })
            ],
        controllers: [AuthController],
        providers: [AuthService]
    }).compile();

    controller = module.get<AuthController>(AuthController);
    spyService = module.get<AuthService>(AuthService);
  });

  it('register', async () => {
    const param : RegisterDto = {
        email: "ayesha.d@gmail.com",
        name: "Ayesha Noor D",
        password: "@yesh4",
        phone: "081246889967"
    };
    // await controller.register(param);
    expect(controller.register(param)).toBeDefined();
  });

  it('register', async () => {
    const param : RegisterDto = {
        email: "",
        name: "Ayesha Noor D",
        password: "@yesh4",
        phone: "081246889967"
    };
    await controller.register(param);
    // await controller.register(param);
    expect(spyService.register(param)).resolves.toBeCalled();
  });

  it('login', async () => {
    const param : AuthDto = {
        email: "ayesha.d@gmail.com",
        password: "@yesh4"
    };
    await controller.login(param);
    expect(spyService.login(param)).resolves.toBeCalled();
  });

});
