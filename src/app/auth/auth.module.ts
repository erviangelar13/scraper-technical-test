import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Config } from '@/helper/config.helper';

@Module({
  imports:[
    JwtModule.register({
      secret: Config.get('SECRET_KEY'),
      signOptions: { expiresIn: Config.get('EXPIRED') },
    })
  ],
  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule {}
