import { IsAlphanumeric, IsEmail, isEmail, IsNotEmpty, IsPhoneNumber } from 'class-validator';
import { UserEntity } from '@/app/entity/user.entity'
import { ApiProperty } from '@nestjs/swagger';

export class AuthDto {
    @ApiProperty()
    @IsNotEmpty()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;
}

export class RegisterDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;

    @ApiProperty()
    @IsAlphanumeric()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsPhoneNumber()
    phone: string;
}

export class AuthResponse {
    name: string;
    token: string;
    refreshtoken: string;
}
export class RegisterReponse {
    id: number
}
