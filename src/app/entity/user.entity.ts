import { Column, Entity, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { EncryptionTransformer } from "typeorm-encrypted";
@Entity("tb_user")
export class UserEntity extends BaseEntity {
    
    @PrimaryGeneratedColumn('increment')
    id?: number;

    @Column("varchar", { unique: true, name: "email", length: 50 })
    email: string;

    @Column({
        name: 'password',
        type: 'varchar',
        length: 128,
        nullable: true,
        default: '',
        transformer: new EncryptionTransformer({
            key: 'e41c966f21f9e1577802463f8924e6a3fe3e9751f201304213b2f845d8841d61',
            algorithm: 'aes-256-cbc',
            ivLength: 16,
            iv: 'ff5ac19190424b1d88f9419ef949ae56'
        })
    })
    password: string | null;

    @Column("varchar", { name: "name", length: 50 })
    name: string;

    @Column("varchar", { name: "phone", nullable: true, length: 15 })
    phone: string | null;

    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}